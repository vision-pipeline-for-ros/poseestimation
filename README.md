# Pose Estimation with OpenCV, ROS and Python
## Sebstian Bergner

This code is for the extrinsic calibration of a camera with a ChAruCo-Board.

### Prerequisites
+ ROS ecosystem
+ OpenCV
+ numpy
+ intrinsic calibrated camera
+ ChAruCo Board (see https://calib.io)

### Usage
1. start roscore
2. start the the camera publisher
2. start poseEstimationRos.py
3. start tfPoseExtension.py
4. examine the output matrixes e.g. in rviz
