#!/usr/bin/env python

__author__ = "Sebastian Bergner"
__credits__ = "Sebastian Bergner", "Simon Haller"
__license__ = "BSD"
__version__ = "1.1"
__maintainer__ = "Sebastian Bergner"
__status__ = "Development"

import rospy
import roslib
import time
import sys
import numpy as np
import tf
import tf2_ros
import geometry_msgs.msg
from geometry_msgs.msg import TransformStamped
from std_msgs.msg import Float64


class TfPoseExtention:
    def __init__(self):
        self.arm_tvec = None  # arm translation vector
        self.arm_rvec = None  # arm rotation vector
        self.cam_tvec = None  # camera translation vector
        self.cam_rvec = None  # camera rotation vector
        self.tvec = None  # camera to world translation vector
        self.rvec = None  # camera to world rotation vector
        self.frame_id = (
            "panda_pattern_image"
        )  # tf link in the corner of the pattern (aruco id=0)
        self.world_id = (
            "world"
        )  # tf link with coordinates (0, 0, 0) or world coordinate
        self.camera_id = "usb_cam"  # tf link of the camera
        self.camera_pattern_id = (
            "pattern_cam"
        )  # tf link of the pattern from the view of the camera

    # calls the tf from the world coordinate to the corner of the pattern
    def get_arm_pose(self, listener):
        try:
            (self.arm_tvec, self.arm_rvec) = listener.lookupTransform(
                self.world_id, self.frame_id, rospy.Time(0)
            )
        except (tf.LookupException, tf.ConnectivityException):
            print("no transform found")

    # calls the tf from the pattern (view from camera) and the camera
    def get_cam_pose(self, listener):
        try:
            (self.cam_tvec, self.cam_rvec) = listener.lookupTransform(
                self.camera_pattern_id, self.camera_id, rospy.Time(0)
            )
        except (tf.LookupException, tf.ConnectivityException):
            print("no transform found")

    # adds the 2 translation vectors and the 2 rotational vectors together
    def combine_vectors(self):
        if (
            self.arm_tvec is not None
            and self.arm_rvec is not None
            and self.cam_tvec is not None
            and self.cam_rvec is not None
        ):
            self.tvec = np.add(self.arm_tvec, self.cam_tvec)
            self.rvec = np.add(self.arm_rvec, self.cam_rvec)
            # print(dict(rvec = self.rvec, tvec = self.tvec))

    # publishes data tf data
    def publish_TF(self):
        if self.tvec is not None and self.rvec is not None:
            publisher = tf2_ros.StaticTransformBroadcaster()

            static_transformStamped = geometry_msgs.msg.TransformStamped()

            static_transformStamped.header.stamp = rospy.Time.now()
            static_transformStamped.header.frame_id = self.frame_id
            static_transformStamped.child_frame_id = self.camera_pattern_id

            static_transformStamped.transform.translation.x = float(
                self.tvec[0]
            )
            static_transformStamped.transform.translation.y = float(
                self.tvec[1]
            )
            static_transformStamped.transform.translation.z = float(
                self.tvec[2]
            )

            quat = tf.transformations.quaternion_from_euler(
                float(self.rvec[0]), float(self.rvec[1]), float(self.rvec[2])
            )
            static_transformStamped.transform.rotation.x = quat[0]
            static_transformStamped.transform.rotation.y = quat[1]
            static_transformStamped.transform.rotation.z = quat[2]
            static_transformStamped.transform.rotation.w = quat[3]

            publisher.sendTransform(static_transformStamped)


def main():
    rospy.init_node("tfCameraPosePublisher", anonymous=True)
    rate = rospy.Rate(1000.0)

    tfPoseExtender = TfPoseExtention()
    try:
        listener = tf.TransformListener()
    except KeyboardInterrupt:
        print("Exit")

    while not rospy.is_shutdown():
        tfPoseExtender.get_arm_pose(listener)
        tfPoseExtender.get_cam_pose(listener)
        tfPoseExtender.combine_vectors()
        tfPoseExtender.publishTF()
        rate.sleep()


if __name__ == "__main__":
    main()
