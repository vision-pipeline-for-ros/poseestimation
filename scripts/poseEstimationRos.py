#!/usr/bin/env python

__author__ = "Sebastian Bergner"
__credits__ = (
    "Sebastian Bergner",
    "Simon Haller",
    "Jakob Hollenstein",
    "Peter Klemperer"
)
__license__ = "BSD"
__version__ = "1.5"
__maintainer__ = "Sebastian Bergner"
__status__ = "Development"

import rospy
import roslib
from sensor_msgs.msg import Image
from sensor_msgs.msg import CameraInfo
import time
import sys
import numpy as np
import cv2
from cv_bridge import CvBridge, CvBridgeError
import cv2.aruco as aruco
import pickle
import tf
import tf2_ros
import geometry_msgs.msg
from geometry_msgs.msg import TransformStamped
from std_msgs.msg import Float64


class PoseEstimation:
    def __init__(self):
        """ dictionary numbers (not all)
                  DICT_4X4_50   = 0,
                  DICT_4X4_100  = 1,
                  DICT_4X4_250  = 2,
                  DICT_4X4_1000 = 3,
                  DICT_5X5_50   = 4,
                  DICT_5X5_100  = 5,
                  DICT_5X5_250  = 6,
                  DICT_5X5_1000 = 7,
                  DICT_6X6_50   = 8,
                  DICT_6X6_100  = 9,
                  DICT_6X6_250  = 10,
                  DICT_6X6_1000 = 11,
                  DICT_7X7_50   = 12,
                  DICT_7X7_100  = 13,
                  DICT_7X7_250  = 14,
                  DICT_7X7_1000 = 15"""
        self.____cv2 = cv2
        self.____camera_settings_info_topic = (
            "/usb_cam/camera_info"
        )  # camera information topic for camera and distortion matrices
        self.__camera_topic = "/usb_cam/image_raw"  # camera image topic
        self.__charuco_height = 10  # charuco board height
        self.__charuco_width = 7  # charuco board width
        self.__charuco_ch_patternlength = (
            0.02
        )  # border length of one black chessboard block
        self.__charuco_aruco_patternlength = (
            0.015
        )  # border length of a aruco marker
        self.__axis_size = 0.08  # size of the drawn axis
        self.__aruco_dict_num = (
            2
        )  # dictionary number corresponding to the aruco marker see above
        self.__aruco_dict = aruco.Dictionary_get(self.__aruco_dict_num)
        self.__charuco_board = aruco.CharucoBoard_create(
            self.__charuco_height,
            self.__charuco_width,
            self.__charuco_ch_patternlength,
            self.__charuco_aruco_patternlength,
            self.__aruco_dict,
        )
        self.__aruco_parameters = aruco.DetectorParameters_create()
        self.__camera_matrix = None
        self.__dist_coeffs = None
        self.__pickle_file = (
            "imagedata.pickle"
        )  # storage location for pickled data
        self.__camera_pattern_id = "pattern_cam"
        self.__debug = False

        self.__bridge = CvBridge()
        self.__cv_image = None
        self.__cam_info_sub = rospy.Subscriber(
            self.__camera_settings_info_topic,
            CameraInfo,
            self.cb_get_camera_info,
        )

        self.__save_img = False
        self.__timestamp_secs = 0
        self.__timestamp_nsecs = 0
        self.__frame_id = 0
        self.__store_num = (
            0
        )  # variable for how many img files are already saved
        self.__store_threshold = 0  # threshold limit for amount of pictures

    # callback method for getting the camera infos (only gets called once)
    def cb_get_camera_info(self, info):
        self.__camera_matrix = info.K
        self.__dist_coeffs = info.D
        self.__camera_matrix = np.reshape(
            self.__camera_matrix, (3, 3)
        )  # converts into right 3x3 array format
        self.__dist_coeffs = np.reshape(self.__dist_coeffs, (1, 5))
        self.__cam_info_sub.unregister()

    # callback method for getting the images and process them
    def callback(self, image_data):
        try:
            self.__cv_image = self.__bridge.imgmsg_to_cv2(image_data)
            self.__cv_image = self.__cv2.cvtColor(
                self.__cv_image, self.__cv2.COLOR_BGR2RGB
            )
            self.__timestamp_nsecs = image_data.header.stamp.nsecs
            self.__timestamp_secs = image_data.header.stamp.secs
            self.__frame_id = image_data.header.frame_id
            self.charuco_pose()
        except CvBridgeError as e:
            print(e)

    # listens on the camera topic
    def listener(self):
        rospy.Subscriber(self.__camera_topic, Image, self.callback())

    # shows the processed image in window
    def show(self, image):
        self.__cv2.imshow("frame", image)
        self.__cv2.waitKey(1)

    # estimates the current position of the board relative to the camera
    def charuco_pose(self):
        num_corners = 0
        retval = False
        frame = self.__cv_image
        gray = self.__cv2.cvtColor(frame, self.__cv2.COLOR_RGB2GRAY)
        corners, ids, rejectedImgPoints = aruco.detectMarkers(
            gray, self.__aruco_dict, parameters=self.__aruco_parameters
        )

        if ids is not None:
            num_corners, ch_corners, ch_ids = aruco.interpolateCornersCharuco(
                corners, ids, gray, self.__charuco_board
            )

        if num_corners > 5:
            aruco.drawDetectedCornersCharuco(
                frame, ch_corners, ch_ids, (0, 0, 255)
            )
            retval, rvec, tvec = aruco.estimatePoseCharucoBoard(
                ch_corners,
                ch_ids,
                self.__charuco_board,
                self.__camera_matrix,
                self.__dist_coeffs,
            )

            if (
                tvec is not None and rvec is not None and self.__debug
            ):  # outputs matrices into the terminal
                print(dict(rvec=rvec, tvec=tvec))

        if retval:
            frame = aruco.drawAxis(
                frame,
                self.__camera_matrix,
                self.__dist_coeffs,
                rvec,
                tvec,
                self.__axis_size,
            )
            if self.__save_img:
                self.store(rvec, tvec)
            self.publishTF(rvec, tvec)
            if self.__debug:
                print(pose_arm_pattern)
        self.show(frame)

    # stores the transformation vector, the rotational
    # vector the frame_id (usb_cam) and timestamps
    def store(self, rvec, tvec):
        if self.store_num < self.__store_threshold:
            self.__store_num += 1
            print(self.__store_num)
            data = (
                tvec,
                rvec,
                self.__frame_id,
                self.__timestamp_secs,
                self.__timestamp_nsecs,
            )
            with open(self.__pickle_file, "a+") as f:
                pickle.dump(data, f)
        else:
            print("Image threshold reached")

    # publishes data tf data
    def publishTF(self, rvec, tvec):

        publisher = tf2_ros.StaticTransformBroadcaster()

        static_transformStamped = geometry_msgs.msg.TransformStamped()

        static_transformStamped.header.stamp = rospy.Time.now()
        static_transformStamped.header.frame_id = self.__frame_id
        static_transformStamped.child_frame_id = self.__camera_pattern_id

        static_transformStamped.transform.translation.x = float(tvec[0])
        static_transformStamped.transform.translation.y = float(tvec[1])
        static_transformStamped.transform.translation.z = float(tvec[2])

        quat = tf.transformations.quaternion_from_euler(
            float(rvec[0]), float(rvec[1]), float(rvec[2])
        )
        static_transformStamped.transform.rotation.x = quat[0]
        static_transformStamped.transform.rotation.y = quat[1]
        static_transformStamped.transform.rotation.z = quat[2]
        static_transformStamped.transform.rotation.w = quat[3]

        publisher.sendTransform(static_transformStamped)


def main():
    poseEstimator = PoseEstimation()
    rospy.init_node("PoseEstimationNode", anonymous=True)
    try:
        poseEstimator.listener()
    except KeyboardInterrupt:
        print("Exit")
    cv2.destroyAllWindows()
    rospy.spin()


if __name__ == "__main__":
    main()
